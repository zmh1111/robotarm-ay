

#ifndef __SBUS_USR_H_
#define __SBUS_USR_H_

//sbus??
#define SBUS_CH 18
uint16_t sbus_ch_trim[SBUS_CH] = {1500,1500,1500,1500,1500,1500,\
															1500,1500,1500,1500,1500,1500,\
															1500,1500,1500,1500,1500,1500};
//sbus????
uint16_t sbus_ch_max[SBUS_CH] = {2100,2100,2100,2100,2100,2100,\
															2100,2100,2100,2100,2100,2100,\
															2100,2100,2100,2100,2100,2100};
//sbus?????
uint16_t sbus_ch_min[SBUS_CH] = {950,950,950,950,950,950,\
															950,950,950,950,950,950,\
															950,950,950,950,950,950};
//sbus?????
uint16_t sbus_ch_deadzone[SBUS_CH] = {25,25,25,25,25,25,\
															25,25,25,25,25,25,\
															25,25,25,25,25,25};

uint16_t sbus_ch_last[SBUS_CH] = {1500,1500,1500,1500,1500,1500,\
															1500,1500,1500,1500,1500,1500,\
															1500,1500,1500,1500,1500,1500};
uint8_t sbus_ch_send_flag[SBUS_CH] = {0};//???????;
uint16_t sbus_ch_delta[SBUS_CH] = {25,5,5,5,5,5,\
															15,15,15,15,15,15,\
															15,15,15,15,15,15};	

#define DRIVER_CH 3  //底盘驱动通道
#define ARM1_CH 4 //机械臂关节1控制通道
#define ARM2_CH 1 //机械臂关节2控制通道
#define ARM3_CH 2 //机械臂关节3控制通道
#define ARM4_CH 14 //机械臂关节4控制通道//sw4

															
#define DRIVER_SPEED 9  //底盘调速通道//9
#define ARM_SPEED 10 //机械臂关节调速通道//11

#define ARM_HOME_CH 17//机械臂一键回中控制通道；
uint8_t arm_home_flag = 0;//复位标志位															
															
															
#define DRIVER_HOME_CH 6//底盘回中控制通道;	

#define EMERG_STOP 8//急停通道	


#define FOCUS_SPEED_CH 13//调速度和方向通道
#define FOCUS_SPEED_CH_MAX 7
#define PHOTO_CH 12//拍照通道	
#define LIGHT_TIME_CH 11//补光灯时间通道

#define WORK_MODE_CH 15//补光灯时间通道
															
															

															




#endif
