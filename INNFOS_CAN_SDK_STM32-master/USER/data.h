#include "bsp.h"
#include "SCA_API.h"
#include "SCA_APP.h"
#include "sbus_usr.h"
#include "usart.h"	
#include <stdio.h>
#include "sys.h"
#include "util.h"
#include "sdio_sdcard.h"
#include "ff.h"  
#include "exfuns.h"    
#include "malloc.h" 

#define SPEED_FIRST 10
#define SPEED_SECOND 20
#define SPEED_THIRD 30

#define LINE_WORD_NUM 50        //每行数据的位数,需校核



/* Variable defines --------------------------------------------------------------*/
uint8_t cmd = 0;					//外部控制命令

float speed_driver = 0.0;
float speed_driver_max = 3000.0;

float speed_arm = 0.0;
float speed_arm_max = 500.0;

uint32_t speed_focus = 0.0;
uint32_t speed_focus_max = 30.0;

int32_t angle_focus = 0.0;

uint8_t focus_buff[16];
uint8_t photo_buff[16];
int photo_device_id='1';
int picture_name_num=1;
uint8_t picture_name[10] = "1234567890"; 

int current_angle=0; 
int angle_flag=0;

typedef struct
{
    int act_num;
    int tra_id ;
    int sec_num;
    int wel_num;
    int rl_num;
    uint16_t dis_num;
    int foc_num;
    int sev1_deg;
    int sev2_deg;
    int sev3_deg;
    int sev4_deg;
    char pic_na[20];
    int rf_id;
    int pic_dl_t;
    int exp_t;
} work_data;        //读取到的目标参数

typedef struct
{
    int act_num;
    int tra_id ;
    int sec_num;
    int wel_num;
    int rl_num;
    uint16_t dis_num;
    int foc_num;
    int sev1_deg;
    int sev2_deg;
    int sev3_deg;
    int sev4_deg;
    char pic_na[20];
    int rf_id;
    int pic_dl_t;
    int exp_t;
} robot_info;       //心跳包的数据参数



//电机状态缓冲变量
uint8_t motor_status_buff[16];
//电机状态变量 0x00 停止  0x01正转 0xff反转
uint8_t motor_status=0;
//电机绝对位置变量
uint32_t motor_absolute_position=0;
//电机相对位置
int motor_relative_position=0;


uint32_t motor_read_times = 0;
//uint8_t motor_rx_cnt = 0;
uint8_t motor_rev_ok = 0;
motor_state motor_rev_data;
uint32_t lighr_cnt=0;

//补光灯延时时间
uint16_t light_time=0;
uint8_t delay_shot_flag = 0;
uint32_t delay_shot_cnt = 0;
uint16_t delay_shot_rc = 10;
uint16_t delay_shot_use = 10;

int m_work_status = 0;		//当前工作状态，0：等待 1：正在工作中

//FAT功能测试：格式化测试，文件写入测试，文件读取测试（基本功能）
FATFS fs1; //FatFs文件系统对象
FIL fnew; //文件对象
FRESULT res_sd;//文件操作结果
UINT fnum; //文件成功读写数量
BYTE ReadBuffer[1024] = {0};
BYTE WriteBuffer[] = "成功移植了FatFs文件系统！\r\n"; //写缓存区
char WriteSdBuffer[4096] = {0}; //写缓存区
uint32_t g_byteCount = 0;	//数据字节计数	
char *writebufpt;			//每次写数据的指针
work_data work_da_1;
robot_info m_info_now;
char m_file_name[20] = "";	//完善文件名获取流程

uint8_t m_work_mode = 0;
uint8_t m_work_pos = 0;
int m_ls_fr_t = 0;
int m_ls_rf_id = 0;



//函数定义
/* Forward Declaration -----------------------------------------------------------*/
static void Log(void);
static void CMD_Handler(uint8_t cmd);

void mymemset1(uint8_t *focus_buff,int len);
void Uart1_SendCMD();
void Uart2_SendCMD();
void my_itoa(int value,uint8_t *buff);
void Uart1_ReadAngle();
uint32_t setSpeedToTarget(uint32_t target);
int32_t setAngleToTarget(int32_t target);
void Motor_Analytic();
void Sbus2Control();
int AutoWork(char* file_name);
int run_motor_server(work_data* m_work_data);       //执行动作
void run_photo_server(work_data* m_work_data);
int GetSysTime();       //TODO 。获取系统时间函数
void heart_beat_server();
void GetRobotInfo(robot_info* work_info);        //TODO 获取系统当前工作状态
int io_capture_server();
int ReadInfrSensor(int d);        //读取红外传感器数据
int ReadRFid();              //读取RFID号 
void GoFo();        //控制前进
void ActStop();     //停止前后运动
void FoldRobot();       //折叠机械臂以通过过人洞
int MotorWork(work_data* m_work_data);     //开始执行机械臂各轴的拍照动作，完成后返回1
void ManualWork();      //手动模式
int sbus_server();     //执行sbus输入       //执行完成返回1
int ctrol_server(work_data* m_work_data);    //执行指令动作       //执行完成返回1
int CheckSbusIn();      //检查Sbus和上一帧的输入是否一致或者是否时空，如果不一致则返回1
int CheckCmdIn();
void SbusParse(work_data* m_work_data);     //解析Sbus数据并存入结构体
void CmdParse(work_data* m_work_data);
void ManualControl();       //手动模式运动控制函数
void arm_bot_server();
void chess_server();
void photo_shot_server();
int ins_take_photo_ctrol_server();
int ins_write_photo_ctrol_serer();
int ins_read_photo_ctrol_server(); 
int update_photo_server();
int add_photo_server();
int del_photo_server();