/*******************************************************************************************************
* @file    sbus.h
* @author  zhoujun
* @version V1.0.0
* @date    2017/11/28     
* @brief   sbus协议头文件，sbus总线协议  100Kbps ,8位，偶校验(even),2位停止位，无流控
*******************************************************************************************************/

#ifndef __SBUS_H_
#define __SBUS_H_

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus


#include <stdio.h>
#include <stdint.h>
#include "usart.h"
//#include "DMA.h"

	
#define SEND_SBUS_TIME		20		//定时发送sbus的周期时间为20ms,在滴答时钟里面实现
	
#define SBUS_STARTBYTE 	   0x0F     //sbus起始byte
#define SBUS_ENDBYTE       0x00     //sbus结束byte	
#define SBUS_LEN 			25      //sbus一帧数据25byte
	
#define SBUS_DIGITAL_CHANNEL_MIN 	341			//数字通道最小值
#define SBUS_DIGITAL_CHANNEL_MAX 	1706		//数字通道最大值
#define SBUS_ANALOG_CHANNEL_MIN		900			//模拟通道最小值
#define SBUS_ANALOG_CHANNEL_MAX 	1900		//模拟通道最大值
	

/* pre-calculate the floating point stuff as far as possible at compile time */
#define SBUS_SCALE_FACTOR ((SBUS_CHANNEL_DEFAULTMAX - SBUS_CHANNEL_DEFAULTMIN) / (SBUS_DIGITAL_CHANNEL_MAX - SBUS_DIGITAL_CHANNEL_MIN))
//#define SBUS_SCALE_OFFSET (int)(SBUS_CHANNEL_DEFAULTMIN - (SBUS_SCALE_FACTOR * SBUS_DIGITAL_CHANNEL_MIN + 0.5f))	
#define SBUS_SCALE_OFFSET			881       	//通道数据固定偏移量

#define SBUS_CHANNEL_DEFAULTMIN     1000		//设定通道最小阈值，默认值
#define SBUS_CHANNEL_DEFAULTMED     1500
#define SBUS_CHANNEL_DEFAULTMAX     2000		//设定通道最大阈值

#define SBUS_OTHERPTZ_DEFAULT       1500    //第三方云台通道的默认数据	
	
	
//临时加的抛投舵机
#define SBUS_JETTISONIN_ON    		1850		//开启
#define SBUS_JETTISONIN_OFF     	1250		//关闭

//抛投舵机通道
#define SBUSCHAN_JETTISONIN			0x01

//网捕枪的舵机
#define SBUS_NETING_ON    		    850		//开启
#define SBUS_NETING_MED             1500        //中间状态
#define SBUS_NETING_OFF     	    2000		//关闭


//SBUS通道定义
//enum e_UAV_SUBSCHAN
//{
//	SBUSCHAN_PTZ_LR		    = 0x01,    //第三方云台左右向 sbus通道
//	SBUSCHAN_PTZ_FB			= 0x02,    //第三方云台前后向 sbus通道
//	SBUSCHAN_PTZ_ROTATE		= 0x03,    //第三方云台旋转sbus通道
//	SBUSCHAN_PTZ_BUTTON		= 0x04,    //第三方云台按键sbus通道
//	SBUSCHAN_THROWPOINT1	= 0x05,    //抛投吊舱挂点1sbus通道	
//	SBUSCHAN_THROWPOINT2    = 0x06,    //抛投吊舱挂点2sbus通道	
//	SBUSCHAN_SEARCH			= 0x07,    //探照灯sbus通道
//	SBUSCHAN_SEARCHPTZ		= 0x08,    //探照吊舱喊话云台sbus通道，阈值范围[1100,1500],默认1500
//	SBUSCHAN_THROWPOINT3	= 0x09,    //抛投吊舱挂点3sbus通道	
//	SBUSCHAN_THROWPOINT4	= 0x0A,    //抛投吊舱挂点4sbus通道	
//	SBUSCHAN_RESERVE1	    = 11,      //预留
//	SBUSCHAN_RESERVE2	    = 12,      //预留
//	SBUSCHAN_RESERVE3	    = 13,      //预留
//	SBUSCHAN_RESERVE4	    = 14,      //预留
//	SBUSCHAN_RESERVE5	    = 15,      //预留
//	SBUSCHAN_RESERVE6	    = 16,      //预留	
//};


//丢帧标记	
enum e_SBUS_STATUS_LOSEFRAME	
{
	SBUS_LOSEFRAME_RESET        = 0, //无丢帧标记
	SBUS_LOSEFRAME_SET      	= 1, //置位丢帧标记
};	

enum e_SBUS_STATUS_FAILSAFE	
{
	SBUS_FAILSAFE_RESET        = 0, //无失控保护标记
	SBUS_FAILSAFE_SET      		= 1, //置位失控保护标记
};

typedef struct
{	
	uint16_t  chanDat[18];      //通道数据
		
}SBUS_CHANNEL;


extern uint8_t g_timeSendSbusFlag; 
extern uint8_t g_sendSbusBuf[SBUS_LEN];
extern uint8_t g_recvSbusFlag;
extern uint8_t g_recvSbusBuf[SBUS_LEN];
extern SBUS_CHANNEL setchanDat;
extern uint16_t g_setchanDat[18];

extern uint16_t sbuschan[18] ;//转换后的18个通道数据
extern uint8_t lossframe;	//丢帧标记
extern uint16_t failsafe;	//失控保护标记

void initSbus(void);
void chanDatInit(void);
void DataTransChToSBus( uint16_t *converChData, uint8_t lossFrame, uint16_t failsafe, uint8_t *sBusData);
int8_t DataTransSBusToCh(const uint8_t *sBusData, uint16_t *chData, uint8_t *lossFrame, uint16_t *failsafe);
void SBUS_write(const uint16_t *chData, uint8_t lossFrame, uint16_t failsafe, uint8_t *sBusData);	
void sbusTimingProcess(void);		
	

#ifdef __cplusplus
}
#endif


#endif