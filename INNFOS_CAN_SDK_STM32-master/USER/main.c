/**
  ******************************************************************************
  * @文	件 ： main.c
  * @作	者 ： INNFOS Software Team
  * @版	本 ： V1.5.1
  * @日	期 ： 2019.9.10
  * @摘	要 ： 主程序入口
  ******************************************************************************/
  
/* Includes ----------------------------------------------------------------------*/
#include "bsp.h"
#include "SCA_API.h"
#include "SCA_APP.h"
#include "sbus_usr.h"
#include "usart.h"	
#include <stdio.h>
#include "sys.h"
#include "util.h"
#include "sdio_sdcard.h"
#include "ff.h"  
#include "exfuns.h"    
#include "malloc.h" 
#include "data.h"

uint32_t delay_cnt = 0;
extern uint8_t motor_sta_r;
extern uint32_t motor_abs_r;//一圈绝对位置
extern int32_t motor_rel_r;//相对位置
extern uint16_t drive_freq_r;//驱动频率；


//多项功能测试
static FRESULT miscellaneous()
{
	DIR dir;
	FATFS *pfs;
	DWORD fre_clust, fre_sect, tot_sect;
	
	printf("\r\n*************************设备信息获取***************************\r\n");
	//获取设备信息和空簇大小
	res_sd = f_getfree("0:", &fre_clust, &pfs);
	
	//计算得到总的扇区个数和空扇区个数
	tot_sect = (pfs->n_fatent - 2) * pfs->csize;
	fre_sect = fre_clust * pfs->csize;
	
	printf("设备总空间：%10lu KB\r\n可用空间：%10lu KB。\r\n", tot_sect*4, fre_sect*4);
	
	printf("\r\n*************************文件定位和格式化写入功能测试***************************\r\n");
	//打开文件，若不存在就创建
	res_sd = f_open(&fnew, "0:FatFs多项功能测试文件.txt", FA_CREATE_ALWAYS | FA_WRITE | FA_READ);
	//文件打开成功
	if(res_sd == FR_OK)
	{
		printf("打开文件成功！开始读取数据！\r\n");
		res_sd= f_write(&fnew, WriteBuffer, sizeof(WriteBuffer), &fnum);
		
		if(res_sd == FR_OK)
		{
			printf("数据写入成功！\r\n");
			printf("数据：%s\r\n", WriteBuffer);
			//文件定位,定位到文件末尾，f_size获取文件大小
			res_sd = f_lseek(&fnew, f_size(&fnew) - 1);
			if(res_sd == FR_OK)
			{
				//在原文件中追加一行内容
				f_printf(&fnew, "在原文件中追加一行内容。\n");
				f_printf(&fnew, "设备总空间：%10lu KB\r\n可用空间：%10lu KB。\r\n", tot_sect*4, fre_sect*4);
				
				//文件定位到起始位置
				res_sd = f_lseek(&fnew, 0);
				
				if(res_sd == FR_OK)
				{
					//打开文件，若不存在就创建
					res_sd = f_open(&fnew, "0:FatFs多项功能测试文件.txt", FA_OPEN_EXISTING | FA_READ);
					//文件打开成功
					if(res_sd == FR_OK)
					{
						printf("打开文件成功！开始读取数据！\r\n");
						res_sd= f_read(&fnew, ReadBuffer, sizeof(ReadBuffer), &fnum);
						
						if(res_sd == FR_OK)
						{
							printf("数据读取成功！\r\n");
							printf("数据：%s\r\n", ReadBuffer);
						}
						else
						{
							printf("数据读取失败！\r\n");
						}
						
						//关闭文件
						f_close(&fnew);
					}
				}
			}
		}
		else
		{
			printf("数据读取失败！\r\n");
		}
		
		//关闭文件
		f_close(&fnew);
	}
	printf("\r\n*************************目录创建和重命名功能测试***************************\r\n");
	//尝试打开目录
	res_sd = f_opendir(&dir, "0:TestDir");
	if(res_sd != FR_OK)
	{
		//打开目录失败，开始创建目录
		res_sd = f_mkdir("0:TestDir");
	}
	else
	{
		//如果目录已经存在，关闭它
		res_sd = f_closedir(&dir);
		//删除文件
		f_unlink("0:FatFs读写测试文件.txt");
	}
	
	if(res_sd == FR_OK)
	{
		//重命名并移动文件
		res_sd = f_rename("0:FatFs多项功能测试文件.txt", "0:/TestDir/FatFs多项功能测试文件.txt");
		
		if(res_sd == FR_OK)
		{
			printf("重命名并移动文件成功！\r\n");
		}
		else
		{
			printf("重命名并移动文件失败！\r\n");
		}
	}
}
 

//double转char
char* GetDoubleStr(double value)
{
    char buf[15] = {0};//长度可以自定义
    sprintf(buf,"%.8f",value);//保留8位小数，不足补0
    int index = 0;
    int len = strlen(buf);
    for(int i = len-1; i>0; i--)
    {
        if(buf[i] == '0')
            continue;
        else
        {
            if(buf[i] == '.') index = i;           
            else index = i+1;
            break;
        }
    }
    buf[index] = '\0';
    return buf;
}

float map_float32(float x, float in_min, float in_max, float out_min, float out_max)
{
		if(x >= in_max) x = in_max;
	if(x <= in_min) x = in_min;

return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

//extern int HAL_GetTick();

/**
  * @功	能	主程序入口
  * @参	数	无
  * @返	回	无
  */
int main(void)
{ 
//	/* 底层驱动初始化 */
	BSP_Init();			

//	/* 等待执行器稳定 */
	delay_ms(500);	

//	/* 串口1打印LOG信息 */
	Log();
	
	/*初始化SCA控制器*/
	SCA_Lookup();
	SCA_Init();
	
	
	//3,自动回中;
	SCA_Homing_usr();

	//4,进入速度控制模式;
	SCA_Vel_Ctrl_Init();
	/* 等待命令传入 */
	
	while(SD_Init())//检测不到SD卡
	{
		printf("SD Card Error!\r\n");
		printf("Please Check!\r\n");
	}
	
	res_sd = f_mount(&fs1,"0:",1); 					//挂载SD卡 
	
	while(1)
	{	
		
		#define SCA_TEST
		#ifdef SCA_TEST
		if(cmd)
		{
			CMD_Handler(cmd);
			cmd = 0;
		}
		else
			delay_ms(10);
		#endif

		//获取SBUS信号，通过sbus的14通道判断工作模式
		Sbus2Control();

		if(m_work_mode == 1)		//进入自动模式
		{
			AutoWork(m_file_name);
		}
		else{		//进入手动模式
			ManualWork();
		}

		if(GetSysTime() - m_ls_fr_t > 500)		//简单调度器，记录时间间隔，超时则发送心跳包
		{
			heart_beat_server();
		} 
		
	}		
		
}     

void ManualWork()
{
	if(CheckSbusIn() == 1)
	{
		SbusParse(&work_da_1);		//如果sbus输入不一致，则解析并存入运动结构体
		sbus_server();		//sbus控制函数
	}
	if(CheckCmdIn() == 1)
	{
		CmdParse(&work_da_1);		//将目标数据放入结构体中
		ctrol_server(&work_da_1);				//指令控制函数
	}

}

/*检查Sbus控制信息是否更新*/
int CheckSbusIn()
{

}

/*检查控制信息是否更新*/
int CheckCmdIn()
{

}

int sbus_server()
{
	arm_bot_server();

	chess_server();

	photo_shot_server();
}

int ctrol_server(work_data* m_work_data)
{
	//以下判断条件需进行完善
	if(1)
	{
		ins_take_photo_ctrol_server();
	}
	if(1)
	{
		ins_write_photo_ctrol_serer();
	}
	if(1)
	{
		ins_read_photo_ctrol_server();
	}

	char photo_na[] = "";
	switch (cmd)		//根据指令执行操作，指令需进行完善
	{
	case 0:	update_photo_server(); break;		//更新拍照点信息
	case 1:	add_photo_server(); break;		//增加拍照点信息
	case 2:	del_photo_server(); break;		//删除拍照点信息
	case 3:	run_photo_server(&work_da_1); break;		//执行拍照点信息,传入目标点数据
	case 4:; break;
	case 5:; break;
	
	default:
		break;
	}
}

int AutoWork(char* file_name)
{
	if(m_work_status == 0)		//等待模式时,读取文件
	{
		//打开文件，若不存在就创建
		res_sd = f_open(&fnew, file_name, FA_OPEN_EXISTING | FA_READ);
		//文件打开成功
		if(res_sd == FR_OK)
		{
			printf("打开文件成功！开始读取数据！\r\n");
			res_sd= f_read(&fnew, ReadBuffer, LINE_WORD_NUM, &fnum);
		
			if(res_sd == FR_OK)
			{
				int i = 0;
				printf("数据读取成功！\r\n");
				printf("数据：%s\r\n", ReadBuffer);

				uint32_t temp = 0;
				temp = ReadBuffer[i + 0];
				temp = temp << 8;
				work_da_1.act_num = temp | ReadBuffer[i + 1];
				m_work_pos = work_da_1.act_num;		//更新当前工作点数据
				work_da_1.tra_id  = ReadBuffer[i + 2];
				work_da_1.sec_num = ReadBuffer[i + 3];
				work_da_1.wel_num = ReadBuffer[i + 4];
				work_da_1.rl_num  = ReadBuffer[i + 5];
				work_da_1.dis_num = ReadBuffer[i + 6];
				work_da_1.dis_num = (work_da_1.dis_num << 8) | ReadBuffer[i + 7];
				work_da_1.foc_num = ReadBuffer[i + 8];
				work_da_1.foc_num = (work_da_1.foc_num << 8) | ReadBuffer[i + 9];
				work_da_1.sev1_deg = ReadBuffer[i + 10];
				work_da_1.sev1_deg = (work_da_1.sev1_deg << 8) | ReadBuffer[i + 11];
				work_da_1.sev2_deg = ReadBuffer[i + 12];
				work_da_1.sev2_deg = (work_da_1.sev2_deg << 8) | ReadBuffer[i + 13];
				work_da_1.sev3_deg = ReadBuffer[i + 14];
				work_da_1.sev3_deg = (work_da_1.sev3_deg << 8) | ReadBuffer[i + 15];
				work_da_1.sev4_deg = ReadBuffer[i + 16];
				work_da_1.sev4_deg = (work_da_1.sev4_deg << 8) | ReadBuffer[i + 17];
				//todo 继续读取其他数据...
				
			}
			else
			{
				printf("数据读取失败！\r\n");
			}
			
			m_work_status = 1;		//更新状态
		}
	}

	//读取完成后开始工作
	if(run_motor_server(&work_da_1) == 1)
	{
		if(fnew.fptr < fnew.fsize - LINE_WORD_NUM)		//如果当前不是最后一行
		{
			fnew.fptr += LINE_WORD_NUM;		//更新读写指针
		}
		else{
			printf("动作执行完成\r\n");
			//关闭文件
			f_close(&fnew);
		}
		
	}	
}

void SbusParse(work_data* m_work_data)
{

}

void CmdParse(work_data* m_work_data)
{

}

//根据结构体组完成下一步动作
int run_motor_server(work_data* m_work_data)
{	
	run_photo_server(m_work_data);		//执行单步拍照指令
}

/*开始完成各轴动作，完成后返回1*/
int MotorWork(work_data* m_work_data)
{
	return 0;
}

/*拍照服务函数*/
void run_photo_server(work_data* m_work_data)
{
	while(m_info_now.rf_id != m_work_data->rf_id)	//rfid不一致，则前进
	{	int t;
		int rfid_now;

		FoldRobot();		//各轴归位穿过过人洞
		GoFo();		//前进
		if(io_capture_server() == 1)
		{
			t += 1;
			if(t <= 4)		//如果外部中断，则连续读取四次RFID数值
			{
				rfid_now = ReadRFid();
				if(rfid_now != m_ls_rf_id)		//滤波，如果连续四次读取不一致，则重新读取
				{
					t = 0;
				}
				m_ls_rf_id = rfid_now;
				if(t == 4)		//如果四次读取的rfid一致，则进行赋值
				{
					m_info_now.rf_id = rfid_now;
				}
			}
			if(rfid_now == m_work_data->rf_id)
			{
				break;	//当rfid一致，需开始读取红外传感器距离，直到达到目标距离
			}
		}

	}

	while(ReadInfrSensor(m_work_data->dis_num) != 1)		//还未到达指定位置，则继续前进
	{
		FoldRobot();	//折叠机械臂
		GoFo();		//继续前进
	}

	m_info_now.dis_num = m_work_data->dis_num;		//更新数据
	ActStop();		//停止运动

	while (MotorWork(m_work_data) != 1)		//等待各轴运动到指定角度
	{
		
	}
	
	if(MotorWork(m_work_data) == 1)
	{
		m_work_status = 0;		//更新状态变量，开始读取下一条数据
	}

}

int ins_take_photo_ctrol_server()
{

}

int ins_write_photo_ctrol_serer()
{

}

int ins_read_photo_ctrol_server()
{

}

int update_photo_server()
{

}

int add_photo_server()
{

}

int del_photo_server()
{

}

void arm_bot_server()
{

}

void chess_server()
{

}

void photo_shot_server()
{

}

void GoFo()
{
}

void ActStop()
{
}

void FoldRobot()
{

}	

/*获取系统时间*/
int GetSysTime()
{

}

/*获取当前的动作参数*/
void GetRobotInfo(robot_info* work_info)
{

}

/*心跳包读取及构造函数*/
void heart_beat_server()
{
	GetRobotInfo(&m_info_now);

}

/*
读取红外传感器数据，正确则返回1
输入参数：目标数据
*/
int ReadInfrSensor(int d)
{

}

/*读取RFID，返回RFID*/
int ReadRFid()
{

}

//外部中断
int io_capture_server()
{

}

/**
  * @功	能	储存文件
  * @参	数	无
  * @返	回	成功返回1 失败返回0
  */
int SaveFile()
{
	return 0;
}

/**
  * @功	能	记录一帧数据
  * @参	数	无
  * @返	回	成功返回1 失败返回0
  */
int RecordPos()
{
	float pos_0, pos_1, pos_2, pos_3, pos_4, pos_5;
	pos_1 = GetPosFromID(1);
	pos_2 = GetPosFromID(2);
	pos_3 = GetPosFromID(3);
	pos_4 = GetPosFromID(4);
	pos_5 = GetPosFromID(5);
	
}

/**
  * @功	能	储存初始化
  * @参	数	无
  * @返	回	成功返回1 失败返回0
  */
int RecordInit(int rec_id)
{
	if(SDFound(rec_id) == 1)		//SD卡内有同名文件
	{
		if(AskOverWrite() == 0)		//询问是否覆盖，如果否
		{
			return 0;
		}
	}
	if(CreatFile(rec_id))		//新建文件
	{
		// m_work_status = 1;
		return 1;		//成功返回1
	}
	return 0;
}

/**
  * @功	能	在储存卡里新建文件
  * @参	数	无
  * @返	回	成功返回1 失败返回0
  */
int CreatFile(int rec_id)
{

	return 0;
}

/**
  * @功	能	寻找储存卡里是否有同名文件
  * @参	数	无
  * @返	回	成功返回1 失败返回0
  */
int SDFound(int rec_id)
{

	return 0;
}

/**
  * @功	能	询问是否覆盖同名文件
  * @参	数	无
  * @返	回	是为1， 否为0
  */
int AskOverWrite()
{
	return 0;
}

/**
  * @功	能	Control Mode
  * @参	数	无
  * @返	回	无
  */
void Sbus2Control()
{
	////////////////////////////
	//sbus接口协议解析
	if(g_recvSbusFlag == 1){
		g_recvSbusFlag = 0;
		DataTransSBusToCh(g_recvSbusBuf, sbuschan, &lossframe, &failsafe);	
		
		speed_driver_max = map_float32(sbuschan[DRIVER_SPEED - 1],1000,2000,1000,8000);

		if(sbuschan[WORK_MODE_CH - 1] > 1500)
		m_work_mode = 1;	//自动模式
		else
		m_work_mode = 0;	//手动模式

		if(m_work_mode == 0)		
		{
			if(abs((sbuschan[DRIVER_CH - 1]- sbus_ch_last[DRIVER_CH - 1])) > sbus_ch_delta[DRIVER_CH - 1]){//有改变量
			sbus_ch_last[DRIVER_CH - 1] = sbuschan[DRIVER_CH - 1];//更新该变量;					
			
			if((sbuschan[DRIVER_CH - 1] > (sbus_ch_trim[DRIVER_CH - 1] -  sbus_ch_deadzone[DRIVER_CH - 1])) && (sbuschan[DRIVER_CH - 1] < (sbus_ch_trim[DRIVER_CH - 1] +  sbus_ch_deadzone[DRIVER_CH - 1]))){
				speed_driver = 0.0;
			}else{
				speed_driver = map_float32(sbuschan[DRIVER_CH - 1],900,2100, -speed_driver_max,speed_driver_max);
			}				
			setVelocity(0x01,speed_driver);//设置速度	
			
		}
		//机械臂关节1
		speed_arm_max = map_float32(sbuschan[ARM_SPEED - 1],900,2100,100,3000);
		
		if(abs((sbuschan[ARM1_CH - 1]- sbus_ch_last[ARM1_CH - 1])) > sbus_ch_delta[ARM1_CH - 1]){//有改变量
			sbus_ch_last[ARM1_CH - 1] = sbuschan[ARM1_CH - 1];//更新该变量;					
			
			if((sbuschan[ARM1_CH - 1] > (sbus_ch_trim[ARM1_CH - 1] -  sbus_ch_deadzone[ARM1_CH - 1])) && (sbuschan[ARM1_CH - 1] < (sbus_ch_trim[ARM1_CH - 1] +  sbus_ch_deadzone[ARM1_CH - 1]))){
				speed_arm = 0.0;
			}else{
				speed_arm = map_float32(sbuschan[ARM1_CH - 1],900,2100, -speed_arm_max,speed_arm_max);
			}				
			setVelocity(0x02,speed_arm);//设置速度					
		}
	//			//机械臂关节2
		if(abs((sbuschan[ARM2_CH - 1]- sbus_ch_last[ARM2_CH - 1])) > sbus_ch_delta[ARM2_CH - 1]){//有改变量
			sbus_ch_last[ARM2_CH - 1] = sbuschan[ARM2_CH - 1];//更新该变量;					
			
			if((sbuschan[ARM2_CH - 1] > (sbus_ch_trim[ARM2_CH - 1] -  sbus_ch_deadzone[ARM2_CH - 1])) && (sbuschan[ARM2_CH - 1] < (sbus_ch_trim[ARM2_CH - 1] +  sbus_ch_deadzone[ARM2_CH - 1]))){
				speed_arm = 0.0;
			}else{
				speed_arm = map_float32(sbuschan[ARM2_CH - 1],900,2100, -speed_arm_max,speed_arm_max);
			}				
			setVelocity(0x03,speed_arm);//设置速度					
		}
	//			
	//			//机械臂关节3
		if(abs((sbuschan[ARM3_CH - 1]- sbus_ch_last[ARM3_CH - 1])) > sbus_ch_delta[ARM3_CH - 1]){//有改变量
			sbus_ch_last[ARM3_CH - 1] = sbuschan[ARM3_CH - 1];//更新该变量;					
			
			if((sbuschan[ARM3_CH - 1] > (sbus_ch_trim[ARM3_CH - 1] -  sbus_ch_deadzone[ARM3_CH - 1])) && (sbuschan[ARM3_CH - 1] < (sbus_ch_trim[ARM3_CH - 1] +  sbus_ch_deadzone[ARM3_CH - 1]))){
				speed_arm = 0.0;
			}else{
				speed_arm = map_float32(sbuschan[ARM3_CH - 1],900,2100, -speed_arm_max,speed_arm_max);
			}				
			setVelocity(0x04,speed_arm);//设置速度					
		}
	//			
	//			//机械臂关节4
		if(abs((sbuschan[ARM4_CH - 1]- sbus_ch_last[ARM4_CH - 1])) > sbus_ch_delta[ARM4_CH - 1]){//有改变量
			sbus_ch_last[ARM4_CH - 1] = sbuschan[ARM4_CH - 1];//更新该变量;					
			
			if((sbuschan[ARM4_CH - 1] > (sbus_ch_trim[ARM4_CH - 1] -  sbus_ch_deadzone[ARM4_CH - 1])) && (sbuschan[ARM4_CH - 1] < (sbus_ch_trim[ARM4_CH - 1] +  sbus_ch_deadzone[ARM4_CH - 1]))){
				speed_arm = 0.0;
			}else{
				speed_arm = map_float32(sbuschan[ARM4_CH - 1],900,2100, -speed_arm_max,speed_arm_max);
			}				
			setVelocity(0x05,speed_arm);//设置速度					
		}		
	//			//急停开关
		if(abs((sbuschan[EMERG_STOP - 1]- sbus_ch_last[EMERG_STOP - 1])) > sbus_ch_delta[EMERG_STOP - 1]){//有改变量
			sbus_ch_last[EMERG_STOP - 1] = sbuschan[EMERG_STOP - 1];//更新该变量;					
			
			if(sbuschan[EMERG_STOP - 1] > 1750){//电机失能
				disableActuator(0x01);
				disableActuator(0x02);
				disableActuator(0x03);
				disableActuator(0x04);
				disableActuator(0x05);
			}else{//使能
				enableAllActuators();
				SCA_Vel_Ctrl_Init();
			}
			
		}		
		if(sbuschan[ARM_HOME_CH - 1] > 1750){//
				if(arm_home_flag == 0){//只运行一次
					arm_home_flag = 1;						
					SCA_Homing_usr();//调用回中函数
				}
			}else{//
					arm_home_flag = 0;
			}

			delay_cnt ++;
			if(delay_cnt - motor_read_times > 40){
					Uart1_ReadAngle();		//发送读取指令
					motor_read_times = delay_cnt;
			}
			
			if(motor_rev_ok == 1){	
				motor_rev_ok = 0;
					
			}

		//摄像头最大速度调节
			
			if(abs((sbuschan[FOCUS_SPEED_CH_MAX - 1]- sbus_ch_last[FOCUS_SPEED_CH_MAX - 1])) > sbus_ch_delta[FOCUS_SPEED_CH_MAX - 1]){//有改变量
			sbus_ch_last[FOCUS_SPEED_CH_MAX - 1] = sbuschan[FOCUS_SPEED_CH_MAX - 1];//更新该变量;					
			if(sbuschan[FOCUS_SPEED_CH_MAX - 1]==1057){
				speed_focus_max =setSpeedToTarget(SPEED_FIRST);
				
			}
			else if(sbuschan[FOCUS_SPEED_CH_MAX - 1]==1507){
				speed_focus_max =setSpeedToTarget(SPEED_SECOND);
			}else if(sbuschan[FOCUS_SPEED_CH_MAX - 1]==1957){
				speed_focus_max =setSpeedToTarget(SPEED_THIRD);
				
			}
				
		}

		//摄像头速度和方向调节
		if(abs((sbuschan[FOCUS_SPEED_CH - 1]- sbus_ch_last[FOCUS_SPEED_CH - 1])) > sbus_ch_delta[FOCUS_SPEED_CH - 1]){//有改变量
			sbus_ch_last[FOCUS_SPEED_CH - 1] = sbuschan[FOCUS_SPEED_CH - 1];//更新该变量;					
			//speed_focus = map_float32(sbuschan[FOCUS_SPEED_CH - 1],900,2100, 0,speed_focus_max);			
			if(sbuschan[FOCUS_SPEED_CH - 1]>1507){
				angle_focus=setAngleToTarget(360);
				
			}else if(sbuschan[FOCUS_SPEED_CH - 1]<1507){
				angle_focus=setAngleToTarget(-360);
			
			}else{
				angle_focus=setAngleToTarget(0);
			
			}
			
			//速度还需要转换
			if((sbuschan[FOCUS_SPEED_CH - 1] > (sbus_ch_trim[FOCUS_SPEED_CH - 1] -  sbus_ch_deadzone[FOCUS_SPEED_CH - 1])) && (sbuschan[FOCUS_SPEED_CH - 1] < (sbus_ch_trim[FOCUS_SPEED_CH - 1] +  sbus_ch_deadzone[FOCUS_SPEED_CH - 1]))){
				speed_focus = 0.0;
			}else{

				if(sbuschan[FOCUS_SPEED_CH - 1]>1507){
					//做了公式转换
					speed_focus = setSpeedToTarget((uint32_t)map_float32(sbuschan[FOCUS_SPEED_CH - 1],1500,2100, 0,speed_focus_max));
				}else if(sbuschan[FOCUS_SPEED_CH - 1]<1507){
					int temp=1507-sbuschan[FOCUS_SPEED_CH - 1];
					speed_focus = setSpeedToTarget((uint32_t)map_float32(1507*2-sbuschan[FOCUS_SPEED_CH - 1],1500,2100, 0,speed_focus_max));
				}else{
					speed_focus =0;
				
				}

			}				
			Uart1_SendCMD();
				//串口发送函数
		}
		
		}

		//摄像头拍照
		if(abs((sbuschan[PHOTO_CH - 1]- sbus_ch_last[PHOTO_CH - 1])) > sbus_ch_delta[PHOTO_CH - 1]){//有改变量
			sbus_ch_last[PHOTO_CH - 1] = sbuschan[PHOTO_CH - 1];//更新该变量;
		
			/////////////////////////
			// A，启动补光灯；
			GPIO_ResetBits(GPIOB,GPIO_Pin_0);//开启补光灯
			delay_ms(100);
			delay_shot_flag = 1;
			// B，硬件触发
			GPIO_ResetBits(GPIOB,GPIO_Pin_1);					
			
			// C，软件拍照
			Uart2_SendCMD();				
		}

		//延时关闭时间设定
		if(abs((sbuschan[LIGHT_TIME_CH - 1]- sbus_ch_last[LIGHT_TIME_CH - 1])) > sbus_ch_delta[LIGHT_TIME_CH - 1]){//有改变量
		sbus_ch_last[LIGHT_TIME_CH - 1] = sbuschan[LIGHT_TIME_CH - 1];//更新该变量;					
		light_time=map_float32(sbuschan[LIGHT_TIME_CH - 1],1000,2000, 0,100);
				
		}
		if(delay_shot_flag == 1){				
			delay_shot_cnt ++;
		}			
			
		//延时关闭
		if(delay_shot_cnt >= light_time){
			GPIO_SetBits(GPIOB,GPIO_Pin_1);
			GPIO_SetBits(GPIOB,GPIO_Pin_0);
			//lighr_cnt=HAL_GetTick();
			delay_shot_cnt = 0;
			delay_shot_flag = 0;
		}
			
	}
}



/**
  * @功	能	串口打印提示信息
  * @参	数	无
  * @返	回	无
  */
static void Log()
{
	printf("\r\n欢迎使用 INNFOS SCA 驱动测试！\r\n");
	printf("详细通信协议参见 INNFOS WIKI ！\r\n");
	printf("发送 1 轮询总线上的执行器ID ！\r\n");
	printf("发送 2 使用默认ID初始化SCA控制器 ！\r\n");
	printf("发送 3 进入位置归零测试程序 ！\r\n");
	printf("发送 4 进入正反转测试程序 ！\r\n");
	printf("发送 5 进入高低速测试程序 ！\r\n");
	printf("发送 6 将执行器关机 ！\r\n");
	printf("发送 7 显示帮助信息 ！\r\n");
}

/**
  * @功	能	串口命令处理函数
  * @参	数	cmd：接收到的指令
  * @返	回	无
  */
static void CMD_Handler(uint8_t cmd)
{
	switch(cmd)
	{
		case 1:
			printf("\r\n执行轮询程序！\r\n");
		
			/* 调用轮询程序 */
			SCA_Lookup();
		
			printf("轮询结束！\r\n");
		break;
		
		case 2:
			printf("\r\nSCA初始化！\r\n");
		
			/* 调用初始化程序 */
			SCA_Init();
		
			/* 等待执行器稳定 */
			delay_ms(500);
		
			printf("SCA初始化结束！\r\n");
			break;
		
		case 3:
			printf("\r\n进入位置归零测试！\r\n");
		
			/* 调用测试程序 位置归零 */
			SCA_Homing();
		
			printf("位置归零测试结束！\r\n");
			break;
			
		case 4:
			printf("\r\n进入正反转切换测试！\r\n");
		
			/* 调用测试程序 正反转切换 */
			SCA_Exp1();
		
			printf("正反转切换测试结束！\r\n");
			break;
		
		case 5:
			printf("\r\n进入高低速切换测试！\r\n");
			
			/* 调用测试程序 高低速切换 */
			SCA_Exp2();
		
			printf("高低速切换测试结束！\r\n");
			break;
		
		case 6:
			printf("\r\n执行器关机！\r\n");
			
			/* 关闭所有执行器 */
			disableAllActuators();
		
			printf("执行器关机结束！\r\n");
			break;
		
		case 7:
			printf("\r\n设置ID ：?\2 r\n");
			printf("\r\n设置ok  2 r\n");
			
		break;
		default:
			Log();
			break;
	}
	
}



void mymemset1(uint8_t *focus_buff,int len){
	for(int i=0;i<16;i++){
		focus_buff[i]=0x30;
	}
}
//调焦函数
void Uart1_SendCMD()
{
	mymemset1(focus_buff,16);
		//协议头
    focus_buff[0] = 0x01;             
    focus_buff[1] = 0x64;              
    focus_buff[2] = 01;              
	
    focus_buff[3] = speed_focus>>24;          //是否需要反馈
    focus_buff[4] = speed_focus>>16;   //datah
    focus_buff[5] = speed_focus>>8;        //datal
	focus_buff[6] = speed_focus; 

	focus_buff[7] = angle_focus>>24; 
	focus_buff[8] = angle_focus>>16; 
	focus_buff[9] = angle_focus>>8; 
	focus_buff[10] = angle_focus; 
	
	focus_buff[11] = 0x00; 
	focus_buff[12] = 0x00; 
	focus_buff[13] = 0x00; 
	uint16_t uCRC=GetCrC(focus_buff,14);
	focus_buff[14]=uCRC;
	focus_buff[15]=uCRC>>8;
	
	//printf("%d",sbuschan[FOCUS_SPEED_CH-1]);
    USART1_SendStr(focus_buff,16);
	//printf("%d\n",setSpeedToTarget(30));
}

//调焦获取角度信息发送读取指令函数
void Uart1_ReadAngle(){
	mymemset1(focus_buff, 16);
	//地址id
	focus_buff[0] = 0x01;
	//电机查询类
	focus_buff[1] = 0x65;
	//读取电机状态
	focus_buff[2] = 0x00;
	
	//保留为0
	focus_buff[3] = 0x00;          //是否需要反馈
    focus_buff[4] = 0x00;   //datah
    focus_buff[5] = 0x00;        //datal
	focus_buff[6] = 0x00;
	
	focus_buff[7] = 0x00;
	focus_buff[8] = 0x00;
	focus_buff[9] = 0x00;
	focus_buff[10] = 0x00;
		
	focus_buff[11] = 0x00;
	focus_buff[12] = 0x00; 
	focus_buff[13] = 0x00; 
	uint16_t uCRC=GetCrC(focus_buff,14);
	focus_buff[14]=uCRC;
	focus_buff[15]=uCRC>>8;
	USART1_SendStr(focus_buff, 16);
}

uint32_t photo_cnt = 0;

void Uart2_SendCMD(){
	
	photo_buff[0]='#';
	photo_buff[1]=photo_device_id;
	photo_buff[2]='n';
	sprintf(&photo_buff[0]+3,"%d*",photo_cnt++);
	//photo_buff[13]='*';
	USART2_SendStr(photo_buff,14);
	
	
}

void my_itoa(int value,uint8_t *buff) {
	mymemset1(buff,10);
	int i=0;
	while(1){
		int temp=value%10;
		buff[i]=temp+'0';
		i++;
		value=value/10;
		if(value==0){
			break;
		}
	}
}

//还要修改*函数
uint32_t setSpeedToTarget(uint32_t target){
	return target*16384/6000;
}
int32_t setAngleToTarget(int32_t target){
	return target*16384/360;
}

//电机状态解析函数
void Motor_Analytic(){

}









