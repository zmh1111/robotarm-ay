/**
  ******************************************************************************
  * @文	件 ： util.h
  * @作	者 ： AYTECH
  * @版	本 ： V1.0.0
  * @日	期 ： 2023.05.01
  * @摘	要 ： 工具函数文件集
  ******************************************************************************/ 
/* Update log --------------------------------------------------------------------*/
//V1.1.0 2019.08.05 所有API调用接口改为ID，与PC SDK保持一致，增加所有参数的读写API
//V1.5.0 2019.08.16 更改数据接收方式（中断接收），加入非阻塞通信功能，适应数据返回慢的
//					情况。加入获取上次关机状态的API，优化开机流程。
//V1.5.1 2019.09.10 增加轮询功能
//V1.5.3 2019.11.15 优化开关机流程

#ifndef __UTIL_H
#define __UTIL_H


/* Includes ----------------------------------------------------------------------*/
#include "stdio.h"
#include "string.h"
#include "stm32f4xx.h" 

typedef struct{
	uint8_t motor_sta;
	uint32_t motor_abs;//一圈绝对位置
	int32_t motor_rel;//相对位置
	uint16_t drive_freq;//驱动频率；
} motor_state;

extern uint8_t GetCrC_CAM(unsigned char *CmmBuf, unsigned char Len);
extern 	uint8_t  usr_str_cpy(unsigned char *dest,unsigned char *source,int start_num,int end_num);
extern void GetCrC_gimbal(unsigned char *CmmBuf, unsigned char Len);
extern uint16_t GetCrC(unsigned char *CmmBuf, unsigned char Len);

extern float usr_map_float32(float x, float in_min, float in_max, float out_min, float out_max);
extern int32_t usr_map_int64(int64_t x, int64_t in_min, int64_t in_max, int64_t out_min, int64_t out_max);
extern int32_t usr_map_int32(int32_t x, int32_t in_min, int32_t in_max, int32_t out_min, int32_t out_max);
extern short usr_map(short x, short in_min, short in_max, short out_min, short out_max);
extern uint16_t usr_map_uint16(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max);

#endif
