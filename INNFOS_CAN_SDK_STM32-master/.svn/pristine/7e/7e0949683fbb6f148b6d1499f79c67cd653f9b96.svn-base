/*******************************************************************************************************
* @file    sbus.cpp
* @author  zhoujun
* @version V1.0.0
* @date    2017/11/28     
* @brief   sbus协议转换，sbus总线协议  100Kbps ,8位，偶校验(even),2位停止位，无流控
*******************************************************************************************************/

#include "stm32f4xx.h"
#include "sbus.h"
//#include "DMA.h"


#define SBUS

//定义是否开启DMA
#define SBUS_DMA 0

//定时发送sbus数据的时间标志
uint8_t g_timeSendSbusFlag = 0;			

//sbus发送数据全局buf，25个字节
uint8_t g_sendSbusBuf[SBUS_LEN] = {0};

//接受一包sbus数据的标志
uint8_t g_recvSbusFlag = 0;			

//sbus接收数据全局buf，25个字节
uint8_t g_recvSbusBuf[SBUS_LEN] = {0};

//sbus通道数据
SBUS_CHANNEL setchanDat;

//18个通道数据缓存区
uint16_t g_setchanDat[18] = {SBUS_CHANNEL_DEFAULTMED};
//uint8_t g_recvSbusBuf[100] = {0};

uint16_t sbuschan[18] = {0};//转换后的18个通道数据
   uint8_t lossframe = 0;	//丢帧标记
	 uint16_t failsafe = 0;	//失控保护标记
/*******************************************************************************************************
*	函 数 名: initSbus
*	功能说明: 初始化sbus
*	形    参：
*	返 回 值: 
*	注    释：其中抛投吊舱，探照/喊话吊舱都采用该sbus接口
********************************************************************************************************/
void initSbus(void)
{
	
#if SBUS_DMA     //如果开启了DMA接收
	
	UART5_DMA_Config();
#else	
	
	//sbus通信串口
    USART_SBUS_Config(USART_SBUS); 

#endif	
	
	//给sbus通道数据赋值
	chanDatInit();
}


/*******************************************************************************************************
*	函 数 名: chanDatInit
*	功能说明: sbus的通道数据初始化
*	形    参：
*	返 回 值: 
*	注    释：所有SBus通道数据都要设置初始值，一般吊舱都为关闭状态
********************************************************************************************************/
void chanDatInit(void)
{
    uint8_t i = 0;
	
	//给sbus通道数据赋值
	for(i=0; i<16; i++)
	{
		g_setchanDat[i] = SBUS_CHANNEL_DEFAULTMED;  //默认最大值1900，都处于关闭状态
	}
	
	
	//数字通道17
	g_setchanDat[16] = SBUS_DIGITAL_CHANNEL_MIN;
	
	//数字通道18
	g_setchanDat[17] = SBUS_DIGITAL_CHANNEL_MIN;
	
	//初始化网捕舵机
    g_setchanDat[0] = SBUS_NETING_OFF;
}

	
/*******************************************************************************************************
*	函 数 名: DataTransChToSBus
*	功能说明: 把18个通道数据以sbus的格式编码
*	形    参：converChData，待转换的18个通道数据
			  lossFrame，丢帧标记，同时接收机会亮红灯
			  failsafe，失控保护标记
			  sBusData，25个sbus数据，长度25，范围[0,255]
*	返 回 值: 
*	注    释：所有通道数据需做限位处理，同时16个模拟通道数据经过公式转换之后才能得到想要的值
              公式：转换后的通道数据 = （通道-固定偏差值）*1.6，这里固定偏差为881
********************************************************************************************************/
void DataTransChToSBus(uint16_t *converChData, uint8_t lossFrame, uint16_t failsafe, uint8_t *sBusData)
{
	uint16_t chData[18] = {0};       //转换后的18个通道数据
	uint8_t sBusBuf[SBUS_LEN] = {0}; //要发送的sbus数据buf,25字节
	float temp = 0.0f;               //临时转换数据
	uint8_t i = 0;
	
	//轮询判断每个通道是否超阈值，需要做限位处理
	for(i=2; i<18; i++)
	{
		if(converChData[i] < SBUS_CHANNEL_DEFAULTMIN)
		{
			converChData[i] = SBUS_CHANNEL_DEFAULTMIN;
		}
		
		if(converChData[i] > SBUS_CHANNEL_DEFAULTMAX)
		{
			converChData[i] = SBUS_CHANNEL_DEFAULTMAX;
		}			
	}

	
	
	/***********根据公式转换通道数据*************************/
	for(i=0; i<16; i++)
	{
        //16个模拟通道数据，sbus模拟通道数据经过公式转换之后才能得到真正想要的数据，公式：转换后的通道数据 = （通道-固定偏差值）*1.6
		temp = ((float)(converChData[i]) - SBUS_SCALE_OFFSET) *1.6f;
		chData[i] = (uint16_t)temp;		
	}
	
	/*****************start byte****************************/
	sBusData[0] = SBUS_STARTBYTE;

	/*****************data byte****************************/
	sBusData[1] = chData[0];                                        //ch1 low 8
	sBusData[2] = ( (chData[1]&0x1f)  << 3 ) + ( chData[0] >>  8 ); //ch2 low 5 + ch1 high 3
	sBusData[3] = ( (chData[2]&0x03)  << 6 ) + ( chData[1] >>  5 ); //ch3 low 2 + ch2 high 6
	sBusData[4] = chData[2]>>2;                                     //ch3 bit9-2(8 bit)
	sBusData[5] = ( (chData[3]&0x7f)  << 1 ) + ( chData[2] >>  10); //ch4 low 7 + ch3 high 1
	sBusData[6] = ( (chData[4]&0x0f)  << 4 ) + ( chData[3] >>  7 ); //ch5 low 4 + ch4 high 4
	sBusData[7] = ( (chData[5]&0x01)  << 7 ) + ( chData[4] >>  4 ); //ch6 low 1 + ch5 high 7
	sBusData[8] = chData[5]>>1;                                     //ch6 bit8-1(8 bit)
	sBusData[9] = ( (chData[6]&0x3f)  << 2 ) + ( chData[5] >>  9 ); //ch7 low 6 + ch6 high 2
	sBusData[10] = ( (chData[7]&0x07)  << 5 ) + ( chData[6] >>  6 ); //ch8 low 3 + ch7 high 5
	sBusData[11] = chData[7]>>3;                                     //ch8 high 8

	sBusData[12] = chData[8];                                        //ch9 low 8
	sBusData[13] = ( (chData[9]&0x1f)  << 3 ) + ( chData[8] >>  8 ); //ch10 low 5 + ch1 high 3
	sBusData[14] = ( (chData[10]&0x03) << 6 ) + ( chData[9] >>  5 ); //ch11 low 2 + ch2 high 6
	sBusData[15] = chData[10]>>2;                                    //ch11 bit9-2(8 bit)
	sBusData[16] = ( (chData[11]&0x7f) << 1 ) + ( chData[10] >> 10); //ch12 low 7 + ch3 high 1
	sBusData[17] = ( (chData[12]&0x0f) << 4 ) + ( chData[11] >> 7 ); //ch13 low 4 + ch4 high 4
	sBusData[18] = ( (chData[13]&0x01) << 7 ) + ( chData[12] >> 4 ); //ch14 low 1 + ch5 high 7
	sBusData[19] = chData[13]>>1;                                    //ch14 bit8-1(8 bit)
	sBusData[20] = ( (chData[14]&0x3f) << 2 ) + ( chData[13] >> 9 ); //ch15 low 6 + ch6 high 2
	sBusData[21] = ( (chData[15]&0x07) << 5 ) + ( chData[14] >> 6 ); //ch16 low 3 + ch7 high 5
	sBusData[22] = chData[15]>>3;                                    //ch16 high 8

	/*****************flag byte****************************/
#ifdef WBUS                                          //WBUS协议
	if( chData[16] > 1024 )
	{
		sBusData[23] |= 0x01;                        //digital ch17
	}
	else
	{
		sBusData[23] &= ~0x01;
	}

	if( chData[17] > 1024 )
	{
		sBusData[23] |= 0x02;                        //digital ch18
	}
	else
	{
		sBusData[23] &= ~0x02;
	}

	if( lossFrame == 1 )                                //frame lost
	{
		sBusBuf[23] |= 0x04;
	}
	else
	{
		sBusBuf[23] &= ~0x04;
	}

	if( failsafe == 1 )                                 //failsafe
	{
		sBusBuf[23] |= 0x08;
	}
	else
	{
		sBusBuf[23] &= ~0x08;
	}
	
	//flag的bit4-bit7为空
	sBusData[23] &= 0x0F;       //清除其他位
#endif
	
#ifdef SBUS                         //SBUS协议
	if( chData[16] > 1024 )
	{
		sBusData[23] |= 0x80;   //digital ch17
	}
	else
	{
		sBusData[23] &= ~0x80;
	}
	
	if( chData[17] > 1024 )
	{
		sBusData[23] |= 0x40;   //digital ch18
	}
	else
	{
		sBusData[23] &= ~0x40;
	}

	if( lossFrame == 1 )        //frame lost
	{
		sBusBuf[23] |= 0x20;
	}
	else
	{
		sBusBuf[23] &= ~0x20;
	}
	
	if( failsafe == 1 )          //failsafe
	{
		sBusBuf[23] |= 0x10;
	}
	else
	{
		sBusBuf[23] &= ~0x10;
	}
	
	//flag的bit0-bit3为空
	sBusData[23] &= 0xF0;  //清除其他位
#endif
	
	/*****************end byte****************************/
	sBusData[24] = SBUS_ENDBYTE;
     	
	//sbus 发送数据  
	USART_transmitBytes(USART_SBUS, sBusData, SBUS_LEN);

}


/*******************************************************************************************************
*	函 数 名: DataTransSBusToCh
*	功能说明: sbus数据格式转换为通道数据
*	形    参：sBusData，25个sbus数据，长度25，范围[0,255]    18个通道数据，长度18，分辨率
			  chData，18个通道数据，长度18，分辨率：4096（0-4095）
			  lossFrame，丢帧标记，同时接收机会亮红灯
			  failsafe，失控保护标记
*	返 回 值: 1：转换正常；
             -1：sbus数据格式错误，帧头或帧尾错误
*	注    释：逆转换公式：转换后的通道数据 = 接收sbus通道数据/1.6+固定偏差值
********************************************************************************************************/
int8_t DataTransSBusToCh(const uint8_t *sBusData, uint16_t *chData, uint8_t *lossFrame, uint16_t *failsafe)
{
	int8_t ReturnFlag = 0;
	float temp = 0.0f;               //临时转换数据
	uint8_t i = 0;
	
	/*****************数据校验****************************/
	if( (sBusData[0] != 0x0F) || (sBusData[24] != 0x00))
	{
		return ReturnFlag = -1;
	}
	
	/*****************data byte****************************/
	chData[0] = ( (sBusData[ 2]&0x07) << 8 ) +  sBusData[ 1];       						//sBus[ 2] low3 + sBus[ 1] low8 
	chData[1] = ( (sBusData[ 3]&0x3F) << 5 ) + (sBusData[ 2] >> 3 );    					//sBus[ 3] low6 + sBus[ 2] high5 
	chData[2] = ( (sBusData[ 5]&0x01) << 10) + (sBusData[ 4] << 2 ) + (sBusData[ 3] >> 6);  //sBus[ 5] low1 + sBus[ 4] low8  + sBus[ 3] high2 
	chData[3] = ( (sBusData[ 6]&0x0F) << 7 ) + (sBusData[ 5] >> 1 );                        //sBus[ 6] low4 + sBus[ 5] high7 
	chData[4] = ( (sBusData[ 7]&0x7F) << 4 ) + (sBusData[ 6] >> 4 );                        //sBus[ 7] low7 + sBus[ 6] high4 
	chData[5] = ( (sBusData[ 9]&0x03) << 9 ) + (sBusData[ 8] << 1 ) + (sBusData[ 7] >> 7);  //sBus[ 9] low2 + sBus[ 8] low8  + sBus[ 7] high1 
	chData[6] = ( (sBusData[10]&0x1F) << 6 ) + (sBusData[ 9] >> 2 );                        //sBus[10] low5 + sBus[ 9] high6 
	chData[7] = ( (sBusData[11]&0xFF) << 3 ) + (sBusData[10] >> 5 );                        //sBus[11] low8 + sBus[10] high3 
	
	chData[8] = ( (sBusData[13]&0x07) << 8 ) +  sBusData[12];                               //sBus[13] low3 + sBus[12] low8 
	chData[9] = ( (sBusData[14]&0x3F) << 5 ) + (sBusData[13] >> 3 );                        //sBus[14] low6 + sBus[13] high5 
	chData[10] = ( (sBusData[16]&0x01) << 10) + (sBusData[15] << 2 ) + (sBusData[14] >> 6); //sBus[16] low1 + sBus[15] low8  + sBus[14] high2 
	chData[11] = ( (sBusData[17]&0x0F) << 7 ) + (sBusData[16] >> 1 );                       //sBus[17] low4 + sBus[16] high7 
	chData[12] = ( (sBusData[18]&0x7F) << 4 ) + (sBusData[17] >> 4 );                       //sBus[18] low7 + sBus[17] high4 
	chData[13] = ( (sBusData[20]&0x03) << 9 ) + (sBusData[19] << 1 ) + (sBusData[18] >> 7); //sBus[20] low2 + sBus[19] low8  + sBus[18] high1 
	chData[14] = ( (sBusData[21]&0x1F) << 6 ) + (sBusData[20] >> 2 );                       //sBus[21] low5 + sBus[20] high6 
	chData[15] = ( (sBusData[22]&0xFF) << 3 ) + (sBusData[21] >> 5 );                       //sBus[22] low8 + sBus[21] high3 
	
	/*****************flag byte****************************/
#ifdef WBUS                                                        //WBUS??
	if( sBusData[23] & 0x01 )                        //digital ch17
	{
		chData[16] = SBUS_DIGITAL_CHANNEL_MAX;
	}
	else
	{
		chData[16] = SBUS_DIGITAL_CHANNEL_MIN;
	}
	
	if( (sBusData[23] & 0x02) >> 1 )        //digital ch18
	{
		chData[17] = SBUS_DIGITAL_CHANNEL_MAX;
	}
	else
	{
		chData[17] = SBUS_DIGITAL_CHANNEL_MIN;
	}

	if( (sBusData[23] & 0x04) >> 2 )        //frame lost
	{
		*lossFrame = 1;
	}
	else
	{
		*lossFrame = 0;
	}
	
	if( (sBusData[23] & 0x08) >> 3 )        //failsafe
	{
		*failsafe = 1;
	}
	else
	{
		*failsafe = 0;
	}
#endif
	
#ifdef SBUS                                                        //SBUS??
	if( (sBusData[23] & 0x80) >> 7 )        //digital ch17
	{
		chData[16] = SBUS_DIGITAL_CHANNEL_MAX;
	}
	else
	{
		chData[16] = SBUS_DIGITAL_CHANNEL_MIN;
	}
	
	if( (sBusData[23] & 0x40) >> 6 )        //digital ch18
	{
		chData[17] = SBUS_DIGITAL_CHANNEL_MAX;
	}
	else
	{
		chData[17] = SBUS_DIGITAL_CHANNEL_MIN;
	}

	if( (sBusData[23] & 0x20) >> 5 )        //frame lost
	{
		*lossFrame = 1;
	}
	else
	{
		*lossFrame = 0;
	}
	
	if( (sBusData[23] & 0x10) >> 4 )        //failsafe
	{
		*failsafe = 1;
	}
	else
	{
		*failsafe = 0;
	}
	#endif
	
	/*********************************************************/
	/***********根据公式转换通道数据*************************/
	for(i=0; i<16; i++)
	{
        //16个模拟通道数据，sbus模拟通道数据经过公式转换之后才能得到真正想要的数据，
		temp = ((float)(chData[i])/1.6f) + SBUS_SCALE_OFFSET;
		chData[i] = (uint16_t)temp;		
	}
	
	
	ReturnFlag = 1;
	
	return ReturnFlag;

}


/*******************************************************************************************************
*	函 数 名: sbusTimingProcess
*	功能说明: 定时处理sbus总线数据
*	形    参：
*	返 回 值: 
*	注    释：
********************************************************************************************************/
void sbusTimingProcess(void)
{
	static uint8_t sBusData[25] = {0}; //发送sbus数据数组，25字节
	static uint16_t sbuschan[18] = {0};//转换后的18个通道数据
	static uint8_t lossframe = 0;	//丢帧标记
	static uint16_t failsafe = 0;	//失控保护标记
	
	//定时发送SBUS信号
	if(g_timeSendSbusFlag == 1)
	{
		g_timeSendSbusFlag = 0;
		//将chan通道数据转换成sbus协议数据发出去
		DataTransChToSBus(g_setchanDat, SBUS_LOSEFRAME_RESET, SBUS_FAILSAFE_RESET, sBusData);
	}

	//如果接收到sbus数据
	if(g_recvSbusFlag == 1)
	{
		g_recvSbusFlag = 0;
		DataTransSBusToCh(g_recvSbusBuf, sbuschan, &lossframe, &failsafe);
		LED1_TOGGLE;
	}
	
}


/*****************************************************************************/
uint16_t values[16]={0};

void sbus_out(uint16_t num_values)
{
    int i=0;
    uint16_t value=0;
    uint8_t byteindex = 1;
    uint8_t offset = 0;
    uint8_t oframe[25] = { 0 };
    memset(oframe,0,25);
    oframe[0]=0x0f;
    oframe[24]=0x00;

    for (i = 0; (i < num_values) && (i < 16); ++i)
    {
        value = (unsigned short)(((values[i] - SBUS_SCALE_OFFSET) / SBUS_SCALE_FACTOR) + .5f);
        if (value > 0x07ff)
        {
            value = 0x07ff;
        }

        while (offset >= 8)
        {
            ++byteindex;
            offset -= 8;
        }

        oframe[byteindex] |= (value << (offset)) & 0xff;
        oframe[byteindex + 1] |= (value >> (8 - offset)) & 0xff;
        oframe[byteindex + 2] |= (value >> (16 - offset)) & 0xff;
        offset += 11;
    }

	USART_transmitBytes(USART_SBUS, oframe, SBUS_LEN);

}



void Process(uint8_t* raw,uint16_t* result)
{
  uint8_t bitsToRead=3; // bitsToRead表示需要从下一个字节中读取多少bit，规律：bitsToRead每次总是增加3
  uint8_t bitsToShift;
  uint8_t startByte=21;
  uint8_t channelId=15;

  do
  {
    result[channelId]=raw[startByte];

    if(bitsToRead<=8)
    {
      result[channelId]<<=bitsToRead;
      bitsToShift=8-bitsToRead;
      result[channelId]+=(raw[startByte-1]>>bitsToShift);
    }
    else
    {
      result[channelId]<<=8;
      result[channelId]+=raw[startByte-1];
      startByte--;
      bitsToRead-=8;
      bitsToShift=8-bitsToRead;
      result[channelId]+=(raw[startByte-1]>>bitsToShift);
    }

    result[channelId]&=0x7FF;

    channelId--;
    startByte--;
    bitsToRead+=3;

  }while(startByte>0);  
}


