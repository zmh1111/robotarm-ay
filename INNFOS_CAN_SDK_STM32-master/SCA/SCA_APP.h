/**
  ******************************************************************************
  * @文	件 ： SCA_APP.h
  * @作	者 ： INNFOS Software Team
  * @版	本 ： V1.5.1
  * @日	期 ： 2019.09.10
  * @摘	要 ： SCA 测试程序
  ******************************************************************************/ 
  
#ifndef __SCA_APP_H
#define __SCA_APP_H
#include "sys.h"

#define CHISS_ZERO 0.0f
#define ARM1_ZERO 0.0f
#define ARM2_ZERO 0.0f
#define ARM3_ZERO -2.1f  //相机水平轴0位值;
#define ARM4_ZERO -11.7f  //相机俯仰轴0位值;



void SCA_Init(void);	//控制器初始化
void SCA_Homing(void);	//执行器归零
void SCA_Exp1(void);  	//正反转Demo
void SCA_Exp2(void);	//高低速Demo
void SCA_Lookup(void);	//查找存在的ID
void SCA_Homing_usr(void);	//执行器归零
void SCA_Vel_Ctrl_Init(void);//设置速度控制模式  
uint8_t SCA_Set_NewID(uint8_t new_id);//设置new ID
float GetPosFromID(int id); //通过id获取位置
#endif


