#ifndef __USART_H
#define __USART_H
#include "stdio.h"	
#include "stm32f4xx_conf.h"
#include "sys.h" 
#include "sbus.h"
	  	
//初始化IO 串口1 
//bound:波特率

extern uint8_t subs_uart_rec_sta;
extern uint8_t sbus_rec_data;
extern uint16_t subs_uart_rec_count;
extern uint32_t g_recvSbusTimems;

#define USART_OSDK_IRQn   		USART3_IRQn   //与DJI OSDK 通信接口
#define USART_SBUS				USART6   //sbus			
//==========BEEP:高电平有效===================
#define GPIO_BEEP			            GPIOE
#define GPIO_PIN_BEEP     	            GPIO_Pin_2
#define BEEP_ON		                    GPIO_SetBits(GPIO_BEEP, GPIO_PIN_BEEP )
#define BEEP_OFF			            GPIO_ResetBits(GPIO_BEEP, GPIO_PIN_BEEP)
#define BEEP_TOGGLE			   			{if(GPIO_ReadOutputDataBit(GPIO_BEEP, GPIO_PIN_BEEP ) == 1){BEEP_OFF;}else{BEEP_ON;}} 
#define READ_BEEP_GPIO                	GPIO_ReadOutputDataBit(GPIO_BEEP, GPIO_PIN_BEEP)

//==========LED :低电平有效===================
#define GPIO_LED1			            GPIOE
#define GPIO_PIN_LED1     	            GPIO_Pin_3
#define LED1_ON		                    GPIO_ResetBits(GPIO_LED1, GPIO_PIN_LED1 )
#define LED1_OFF			            GPIO_SetBits(GPIO_LED1, GPIO_PIN_LED1)
#define LED1_TOGGLE			    		{if(GPIO_ReadOutputDataBit(GPIO_LED1, GPIO_PIN_LED1 ) == RESET){LED1_OFF;}else{LED1_ON;}} 

#define GPIO_LED2			            GPIOE
#define GPIO_PIN_LED2     	            GPIO_Pin_4
#define LED2_ON		                    GPIO_ResetBits(GPIO_LED2, GPIO_PIN_LED2 )
#define LED2_OFF			            GPIO_SetBits(GPIO_LED2, GPIO_PIN_LED2)
#define LED2_TOGGLE			    		{if(GPIO_ReadOutputDataBit(GPIO_LED2, GPIO_PIN_LED2 ) == RESET){LED2_OFF;}else{LED2_ON;}} 

#define GPIO_LED3			            GPIOE
#define GPIO_PIN_LED3     	            GPIO_Pin_5
#define LED3_ON		                    GPIO_ResetBits(GPIO_LED3, GPIO_PIN_LED3 )
#define LED3_OFF			            GPIO_SetBits(GPIO_LED3, GPIO_PIN_LED3)
#define LED3_TOGGLE			    		{if(GPIO_ReadOutputDataBit(GPIO_LED3, GPIO_PIN_LED3 ) == RESET){LED3_OFF;}else{LED3_ON;}} 


//==========干扰枪网捕枪:低电平有效===================
#define GPIO_GAM1500M			        GPIOA
#define GPIO_PIN_GAM1500M     	        GPIO_Pin_4
#define GAM1500M_OFF		            GPIO_ResetBits(GPIO_GAM1500M, GPIO_PIN_GAM1500M )
#define GAM1500M_ON			            GPIO_SetBits(GPIO_GAM1500M, GPIO_PIN_GAM1500M)
#define GAM1500M_TOGGLE			    	{if(GPIO_ReadOutputDataBit(GPIO_GAM1500M, GPIO_PIN_GAM1500M ) == RESET){GAM1500M_OFF;}else{GAM1500M_ON;}} 

#define GPIO_GAM24G			            GPIOA
#define GPIO_PIN_GAM24G     	        GPIO_Pin_5
#define GAM24G_OFF		                GPIO_ResetBits(GPIO_GAM24G, GPIO_PIN_GAM24G )
#define GAM24G_ON			        	GPIO_SetBits(GPIO_GAM24G, GPIO_PIN_GAM24G)
#define GAM24G_TOGGLE			    	{if(GPIO_ReadOutputDataBit(GPIO_GAM24G, GPIO_PIN_GAM24G ) == RESET){GAM24G_OFF;}else{GAM24G_ON;}} 

#define GPIO_GAM58G			        	GPIOA
#define GPIO_PIN_GAM58G    	            GPIO_Pin_6
#define GAM58G_OFF		                GPIO_ResetBits(GPIO_GAM58G, GPIO_PIN_GAM58G )
#define GAM58G_ON			        	GPIO_SetBits(GPIO_GAM58G, GPIO_PIN_GAM58G)
#define GAM58G_TOGGLE			    	{if(GPIO_ReadOutputDataBit(GPIO_GAM58G, GPIO_PIN_GAM58G ) == RESET){GAM58G_OFF;}else{GAM58G_ON;}} 

#define GPIO_NETING1			        GPIOA
#define GPIO_PIN_NETING1    	        GPIO_Pin_7
#define NETING1_ON		                GPIO_ResetBits(GPIO_NETING1, GPIO_PIN_NETING1 )
#define NETING1_OFF			        	GPIO_SetBits(GPIO_NETING1, GPIO_PIN_NETING1)
#define NETING1_TOGGLE			    	{if(GPIO_ReadOutputDataBit(GPIO_NETING1, GPIO_PIN_NETING1 ) == RESET){NETING1_OFF;}else{NETING1_ON;}} 



void Gpio_Config(void);

void initBSP();


void uart_init(uint32_t bound);
char USART1_Read(void);
void USART1_Send(char ch);

void Gpio_Config(void);
void USART1_Gpio_Config(void);
void USART2_Gpio_Config(void);
void USART3_Gpio_Config(void);
void UART4_Gpio_Config(void);
void UART5_Gpio_Config(void);
void USART6_Gpio_Config(void);
void UART7_Gpio_Config(void);
void UART8_Gpio_Config(void);
void USART_SBUS_Config(USART_TypeDef* USARTx);
void USART_SBUS_IRQn(void);
void USART2_Send(char ch);
void USART2_SendStr(uint8_t *pData, uint16_t Len);
//void USART6_IRQn(void);	

void USARTx_SendByte(USART_TypeDef* USARTx, uint8_t data);
void USART_transmitBytes(USART_TypeDef* USARTx, uint8_t* bytes, uint16_t len);

void                      USART2_Config(void);
void                      USART3_Config(void);
void                      USARTxNVIC_Config(void);
void                      UsartConfig(void);
void                      NVIC_Config(void);
void                      Rx_buff_Handler();
void USARTx_Config(USART_TypeDef* USARTx, uint32_t baud);


#endif


