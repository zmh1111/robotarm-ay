#include "sys.h"
#include "usart.h"	
#include "util.h"
#include <stdlib.h>

uint8_t subs_uart_rec_sta = 0;
uint8_t sbus_rec_data = 0;
uint16_t subs_uart_rec_count = 0;
uint32_t g_recvSbusTimems = 0;


//电机状态缓冲量
extern uint8_t motor_status_buff[16];

//////////////////////////////////////////////////////////////////
//加入以下代码,支持printf函数,而不需要选择use MicroLIB	  
#if 1
#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 
}; 

FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
void _sys_exit(int x) 
{ 
	x = x; 
} 
//重定义fputc函数 //改成了串口一本来是三
int fputc(int ch, FILE *f)
{ 	
	while((USART3->SR&0X40)==0);//循环发送,直到发送完毕   
	USART3->DR = (uint8_t) ch;      
	return ch;
}
#endif
 
//串口1 GPIO初始化
void USART1_Gpio_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);   

	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9 | GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1); // tx
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1); // rx
}

//串口2 GPIO初始化
void USART2_Gpio_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2); // tx
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2); // rx
}

//串口3 GPIO初始化
void USART3_Gpio_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOD, GPIO_PinSource8, GPIO_AF_USART3); // tx
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource9, GPIO_AF_USART3); // rx
}

//串口4 GPIO初始化
void UART4_Gpio_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_0 | GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_UART4); // tx
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_UART4); // rx
}

//串口5 GPIO初始化
void UART5_Gpio_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);
	
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_12;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_UART5); // tx
    GPIO_PinAFConfig(GPIOD, GPIO_PinSource2, GPIO_AF_UART5);  // rx
}

//串口6 GPIO初始化
void USART6_Gpio_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);
	
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_USART6); // tx
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_USART6); // rx
}

//串口7 GPIO初始化
void UART7_Gpio_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART7, ENABLE);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_7 | GPIO_Pin_8;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOE, GPIO_PinSource7, GPIO_AF_UART7); // rx
    GPIO_PinAFConfig(GPIOE, GPIO_PinSource8, GPIO_AF_UART7); // tx
}

//串口8 GPIO初始化
void UART8_Gpio_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART8, ENABLE);
	
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_0 | GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOE, GPIO_PinSource0, GPIO_AF_UART8); // rx
    GPIO_PinAFConfig(GPIOE, GPIO_PinSource1, GPIO_AF_UART8); // tx
}


/*******************************************************************************************************
*	函 数 名: Gpio_Config
*	功能说明: 普通GPIO初始化
*	形    参：
*	返 回 值: 
*	注    释：
********************************************************************************************************/
void Gpio_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC | 
                            RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOE | RCC_AHB1Periph_GPIOG, ENABLE);

	//蜂鸣器
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_BEEP;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_BEEP, &GPIO_InitStructure);
	BEEP_OFF;
	
	//LED
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_LED1;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_LED1, &GPIO_InitStructure);
	LED1_OFF;
	
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_LED2;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_LED2, &GPIO_InitStructure);
	LED2_OFF;
	
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_LED3;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_LED3, &GPIO_InitStructure);
	LED3_OFF;

    //干扰枪
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_GAM1500M;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_GAM1500M, &GPIO_InitStructure);
	GAM1500M_OFF;
	
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_GAM24G;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_GAM24G, &GPIO_InitStructure);
	GAM24G_OFF;
	
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_GAM58G;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_GAM58G, &GPIO_InitStructure);
	GAM58G_OFF;
	
	//网捕枪
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_NETING1;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_NETING1, &GPIO_InitStructure);
	NETING1_OFF;
    

#if 0
	//三色LED
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_LEDR;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_LEDR, &GPIO_InitStructure);
	LEDR_OFF;
	
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_LEDG;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_LEDG, &GPIO_InitStructure);
	LEDG_OFF;
	
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_LEDB;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_LEDB, &GPIO_InitStructure);
	LEDB_OFF;
	
	
	//P900配置引脚
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_MCU_RESET;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_MCU_RESET, &GPIO_InitStructure);
	MCU_RESET_OFF;
	
	GPIO_InitStructure.GPIO_Pin   = GPIO_PIN_MCU_CONFIG;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(GPIO_MCU_CONFIG, &GPIO_InitStructure);
	MCU_CONFIG_OFF;
#endif

	//串口引脚配置
	USART1_Gpio_Config();
	USART2_Gpio_Config();
	USART3_Gpio_Config();
	UART5_Gpio_Config();
	UART7_Gpio_Config();
	UART8_Gpio_Config();
	
}


/*******************************************************************************************************
*	函 数 名: toggleBeep
*	功能说明: 滴滴两声
*	形    参：无
*	返 回 值: 
*	注    释：初始化OSDK成功后滴滴两声提示   
********************************************************************************************************/
void toggleBeep(void)
{
    BEEP_ON;
	delay_ms(500);
	BEEP_OFF;
	delay_ms(500);
    
	BEEP_ON;
    delay_ms(500);
    BEEP_OFF;
    
}


/**********************************************************************************************************
*	函 数 名: USARTx_SendByte
*	功能说明: 发送1个字节，查询发送
*	形    参：USARTx：串口数据结构体
              data：一字节数据
*	返 回 值: 无
*	注    释：
**********************************************************************************************************/
void USARTx_SendByte(USART_TypeDef* USARTx, uint8_t data)
{
	USART_ClearFlag(USARTx,USART_FLAG_TC);
  	USART_SendData(USARTx, data);
  	while(USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET);
}


/**********************************************************************************************************
*	函 数 名: USART_transmitBytes
*	功能说明: 发送多个字节
*	形    参：USARTx：串口号
              bytes：多字节数据，一般为数组
              len：发送数组长度
*	返 回 值: 无
*	注    释：
**********************************************************************************************************/
void USART_transmitBytes(USART_TypeDef* USARTx, uint8_t* bytes, uint16_t len)
{
	uint16_t i = 0;
	for(i=0; i<len; i++)
	{
		USART_ClearFlag(USARTx,USART_FLAG_TC);
		USART_SendData(USARTx,bytes[i]);
		while(USART_GetFlagStatus(USARTx, USART_FLAG_TC)==RESET);		
	}
}

/*******************************************************************************************************
*	函 数 名: USARTx_Config
*	功能说明: 串口初始化
*	形    参：USARTx，串口结构体
			  baud，波特率
*	返 回 值: 
*	注    释：
********************************************************************************************************/
void USARTx_Config(USART_TypeDef* USARTx, uint32_t baud)
{

    USART_InitTypeDef USART_InitStructure;
    
    USART_InitStructure.USART_BaudRate   = baud;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits   = USART_StopBits_1;
    USART_InitStructure.USART_Parity     = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl =
    USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    USART_Init(USARTx, &USART_InitStructure);
    USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);

    USART_Cmd(USARTx, ENABLE);
    while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) != SET);
}




void USARTxNVIC_Config()
{
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
#if 0 
	//Z36T
    NVIC_InitTypeDef NVIC_InitStructure_USART1;
    NVIC_InitStructure_USART1.NVIC_IRQChannelPreemptionPriority = 0x00; //抢占优先级
    NVIC_InitStructure_USART1.NVIC_IRQChannelSubPriority        = 0x02; //子优先级
    NVIC_InitStructure_USART1.NVIC_IRQChannel                   = USART_Z36TPTZ_IRQn;
    NVIC_InitStructure_USART1.NVIC_IRQChannelCmd                = ENABLE;
    NVIC_Init(&NVIC_InitStructure_USART1);
#endif  

#if 1
	//避障雷达	
    NVIC_InitTypeDef NVIC_InitStructure_USART2;
    NVIC_InitStructure_USART2.NVIC_IRQChannelPreemptionPriority = 0x03;
    NVIC_InitStructure_USART2.NVIC_IRQChannelSubPriority        = 0x02;
    NVIC_InitStructure_USART2.NVIC_IRQChannel                   = USART2_IRQn;
    NVIC_InitStructure_USART2.NVIC_IRQChannelCmd                = ENABLE;
    NVIC_Init(&NVIC_InitStructure_USART2);
#endif 

#if 1 
	//OSDK
    NVIC_InitTypeDef NVIC_InitStructure_USART3;
    NVIC_InitStructure_USART3.NVIC_IRQChannelPreemptionPriority = 0x03; //抢占优先级
    NVIC_InitStructure_USART3.NVIC_IRQChannelSubPriority        = 0x01; //子优先级
    NVIC_InitStructure_USART3.NVIC_IRQChannel                   = USART3_IRQn;
    NVIC_InitStructure_USART3.NVIC_IRQChannelCmd                = ENABLE;
    NVIC_Init(&NVIC_InitStructure_USART3);
#endif 

#if 0 
	//生命探测仪
    NVIC_InitTypeDef NVIC_InitStructure_UART4;
    NVIC_InitStructure_UART4.NVIC_IRQChannelPreemptionPriority  = 0x02;
    NVIC_InitStructure_UART4.NVIC_IRQChannelSubPriority         = 0x02;
    NVIC_InitStructure_UART4.NVIC_IRQChannel                    = USART_LIFEDETECT_IRQn;
    NVIC_InitStructure_UART4.NVIC_IRQChannelCmd                 = ENABLE;
    NVIC_Init(&NVIC_InitStructure_UART4);
#endif 

#if 0 
	
    NVIC_InitTypeDef NVIC_InitStructure_UART5;
    NVIC_InitStructure_UART5.NVIC_IRQChannelPreemptionPriority  = 0x04;
    NVIC_InitStructure_UART5.NVIC_IRQChannelSubPriority         = 0x03;
    NVIC_InitStructure_UART5.NVIC_IRQChannel                    = UART5_IRQn;
    NVIC_InitStructure_UART5.NVIC_IRQChannelCmd                 = ENABLE;
    NVIC_Init(&NVIC_InitStructure_UART5);
#endif 

#if 1  
	//SBUS
    NVIC_InitTypeDef NVIC_InitStructure_USART6;
    NVIC_InitStructure_USART6.NVIC_IRQChannelPreemptionPriority  = 0x02;
    NVIC_InitStructure_USART6.NVIC_IRQChannelSubPriority         = 0x02;
    NVIC_InitStructure_USART6.NVIC_IRQChannel                    = USART6_IRQn;
    NVIC_InitStructure_USART6.NVIC_IRQChannelCmd                 = ENABLE;
    NVIC_Init(&NVIC_InitStructure_USART6);
#endif    

#if 0 
	//高度计
    NVIC_InitTypeDef NVIC_InitStructure_UART7;
    NVIC_InitStructure_UART7.NVIC_IRQChannelPreemptionPriority  = 0x04;
    NVIC_InitStructure_UART7.NVIC_IRQChannelSubPriority         = 0x04;
    NVIC_InitStructure_UART7.NVIC_IRQChannel                    = USART_ALTIMETER_IRQn;
    NVIC_InitStructure_UART7.NVIC_IRQChannelCmd                 = ENABLE;
    NVIC_Init(&NVIC_InitStructure_UART7);
#endif 

#if 0   
	//pixy通信串口
    NVIC_InitTypeDef NVIC_InitStructure_UART8;
    NVIC_InitStructure_UART8.NVIC_IRQChannelPreemptionPriority  = 0x04;
    NVIC_InitStructure_UART8.NVIC_IRQChannelSubPriority         = 0x04;
    NVIC_InitStructure_UART8.NVIC_IRQChannel                    = USART_PIXY_IRQn;
    NVIC_InitStructure_UART8.NVIC_IRQChannelCmd                 = ENABLE;
    NVIC_Init(&NVIC_InitStructure_UART8);
#endif 

}

/**********************************************************************************************************
*	函 数 名: USART3_IRQHandler
*	功能说明: 串口3的中断处理函数
*	形    参：
*	返 回 值: 
*	注    释：大疆数据链OSDK的通信接口
**********************************************************************************************************/
uint8_t cmd_uart2 = 0;

void USART2_IRQHandler(void)
{
    //增加溢出中断处理，否则会导致进入硬件错误，程序奔溃

		if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)  
	{
		//shellHandler(&SCA_Shell,USART1->DR);
		if(cmd_uart2 == 0)	
			cmd_uart2 = USART2->DR;
	}
	
}
extern uint8_t cmd;

/**********************************************************************************************************
*	函 数 名: USART3_IRQHandler
*	功能说明: 串口3的中断处理函数
*	形    参：
*	返 回 值: 
*	注    释：大疆数据链OSDK的通信接口
**********************************************************************************************************/
void USART3_IRQHandler(void)
{
    //增加溢出中断处理，否则会导致进入硬件错误，程序奔溃

		if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)  
	{
		//shellHandler(&SCA_Shell,USART1->DR);
		if(cmd == 0)	
			cmd = USART3->DR;
	}
	
}




#define USART_REC_LEN  			200  	//定义最大接收字节数 200
#define EN_USART1_RX 			1		//使能（1）/禁止（0）串口1接收
	  	
//第一位开始接收
u16 USART_RX_STA=0;       //接收状态标记	
u8 USART_RX_BUF[USART_REC_LEN];     //接收缓冲,最大USART_REC_LEN个字节.

uint8_t g_usart_rx_buf[200];
uint16_t g_usart_rx_sta = 0;
int num=0;

uint16_t CrC;
uint8_t motor_rx_state = 0;

//typedef struct{
//	uint8_t motor_sta;
//	uint32_t motor_abs;//一圈绝对位置
//	int32_t motor_rel;//相对位置
//	uint16_t drive_freq;//驱动频率；
//} motor_state;

extern motor_state motor_rev_data;

uint8_t motor_rx_buff[50];
uint8_t motor_rx_cnt = 0;
extern uint8_t motor_rev_ok ;

uint8_t motor_sta_r;
uint32_t motor_abs_r;//一圈绝对位置
int32_t motor_rel_r;//相对位置
uint16_t drive_freq_r;//驱动频率；

	u8 Res;

void USART1_IRQHandler(void)                	//串口1中断服务程序
{
//	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)  
//	{
////		shellHandler(&SCA_Shell,USART1->DR);
////		if(cmd == 0)	
////			cmd = USART1->DR;
//		
//	}
//	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)  //接收中断(接收到的数据必须是0x0d 0x0a结尾)
//	{
//		u8 Res;
//		Res =USART_ReceiveData(USART1);//(USART1->DR);	//读取接收到的数据
//		
//		for (int i=0;i<14;i++){
//			motor_status_buff[i]=Res;
//		}  		 

//		uint16_t uCRC=GetCrC(motor_status_buff,14);
//		motor_status_buff[14]=uCRC;
//		motor_status_buff[15]=uCRC>>8;
//  }

	if(USART_GetFlagStatus(USART1, USART_FLAG_ORE) != RESET)
	{	
		USART_ClearFlag(USART1, USART_FLAG_ORE); 
		USART_ReceiveData(USART1);	
	}
	
    if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);        //清除中断标志
		//	sbus_rec_data = USART_ReceiveData(USART1);
	//if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)  //接收中断(接收到的数据必须是0x0d 0x0a结尾)
	//{
		Res =USART_ReceiveData(USART1);//(USART1->DR);	//读取接收到的数据
	

    while((USART3->SR&0X40)==0);	//循环发送,直到发送完毕   
	  USART3->DR = Res;  			
			
			
		//开始接收
		switch(motor_rx_state){
		
			case 0:
					if(Res == 0x28){
					motor_rx_state = 1;
					motor_rx_cnt = 0;
							//motor_rev_ok = 1;
					}			
			break;
			
			case 1:
				if(Res == 0x2C){ //第一个逗号
					
					uint8_t str_temp[motor_rx_cnt];
					memset(str_temp,0,motor_rx_cnt);
					memcpy(str_temp,motor_rx_buff,motor_rx_cnt);
					motor_sta_r = atoi(&str_temp[0]);
					
					motor_rx_cnt = 0;
					memset(motor_rx_buff,0,50);
					motor_rx_state = 2;
					
				}else{					
						motor_rx_buff[motor_rx_cnt++] = Res;					
				}
			break;
				
				case 2:
				if(Res == 0x2C){ //第二个逗号
				
					uint8_t str_temp[motor_rx_cnt];
					memset(str_temp,0,motor_rx_cnt);
					memcpy(str_temp,motor_rx_buff,motor_rx_cnt);
					motor_abs_r = atoi(&str_temp[0]);
					
					motor_rx_cnt = 0;
					memset(motor_rx_buff,0,50);
					motor_rx_state = 3;//转换到下一个流程
					
				}else{					
						motor_rx_buff[motor_rx_cnt++] = Res;					
				}
			break;
				
				case 3:
				if(Res == 0x2C){ //第三个逗号
				
					uint8_t str_temp[motor_rx_cnt];
					memset(str_temp,0,motor_rx_cnt);
					memcpy(str_temp,motor_rx_buff,motor_rx_cnt);
					motor_rel_r = atoi(&str_temp[0]);
					
					motor_rx_cnt = 0;
					memset(motor_rx_buff,0,50);
					motor_rx_state = 4;
					
				}else{					
						motor_rx_buff[motor_rx_cnt++] = Res;					
				}
			break;
				
				case 4:
				if(Res == 0x29){ //结尾右括号
				
					uint8_t str_temp[motor_rx_cnt];
					memset(str_temp,0,motor_rx_cnt);
					memcpy(str_temp,motor_rx_buff,motor_rx_cnt);
					drive_freq_r = atoi(&str_temp[0]);
					
					motor_rx_cnt = 0;
					memset(motor_rx_buff,0,50);
					motor_rx_state = 0;
					motor_rev_ok = 1;
					
					
				}else{					
						motor_rx_buff[motor_rx_cnt++] = Res;					
				}
			break;
				
//			
//			case 2:
//				if(Res == 0x00){
//					motor_rx_state = 3;
//					}
//				
//			break;
//			
//			case 3:
//				// if(motor_rx_cnt <= 13){	
//			   motor_rx_buff[motor_rx_cnt ++] = Res;
//				 if(motor_rx_cnt == 13){
//					memcpy(&motor_rev_data,motor_rx_buff,13);
//					motor_rev_ok = 1;					 
//					motor_rx_state = 0;
//				 }
//			
//			break;
			
		
		
		}
		
		
  }
} 	

void USART1_Send(char ch)
{
	while((USART1->SR&0X40)==0);	//循环发送,直到发送完毕   
	USART1->DR = (uint8_t) ch;  
}

void USART1_SendStr(uint8_t *pData, uint16_t Len)
{
	while(Len--)
	{
		USART1_Send(*pData);
		pData++;
	}
}

char USART1_Read()
{
	if(USART_GetFlagStatus(USART1,USART_FLAG_RXNE) != RESET)
	{
		USART_ClearFlag(USART1,USART_FLAG_RXNE);
		return USART1->DR;
	}
	else
		return 0;
}


void USART2_Send(char ch)
{
	while((USART2->SR&0X40)==0);	//循环发送,直到发送完毕   
	USART2->DR = (uint8_t) ch;  
}

void USART2_SendStr(uint8_t *pData, uint16_t Len)
{
	while(Len--)
	{
		USART2_Send(*pData);
		pData++;
	}
}





#if 1
//sbus串口 sbus总线协议  100Kbps ,8位，偶校验(even),2位停止位，无流控
void USART_SBUS_Config(USART_TypeDef* USARTx)
{
	USART_InitTypeDef USART_InitStructure;

	USART_InitStructure.USART_BaudRate   = 100000;                	//100Kbps
	USART_InitStructure.USART_WordLength = USART_WordLength_9b;		//字长9位，包括校验位
	USART_InitStructure.USART_StopBits   = USART_StopBits_2;		//2位停止位
	USART_InitStructure.USART_Parity     = USART_Parity_Even;		//偶校验
	USART_InitStructure.USART_HardwareFlowControl =
	USART_HardwareFlowControl_None;								    //无流控
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USARTx, &USART_InitStructure);
	USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);

	USART_Cmd(USARTx, ENABLE);
	while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) != SET);
}



void uart_init(uint32_t bound)
{
	//GPIO端口设置
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE); //使能GPIOA时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);//使能USART1时钟

	//串口1对应引脚复用映射
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource9,GPIO_AF_USART1); //GPIOA9复用为USART1
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource10,GPIO_AF_USART1); //GPIOA10复用为USART1
	
	//USART1端口配置
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10; //GPIOA9与GPIOA10
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;//复用功能
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	//速度50MHz
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; //推挽复用输出
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; //上拉
	GPIO_Init(GPIOA,&GPIO_InitStructure); //初始化PA9，PA10

	//USART1 初始化设置
	USART_InitStructure.USART_BaudRate = bound;//波特率设置
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
	USART_Init(USART1, &USART_InitStructure); //初始化串口1

	//Usart1 NVIC 配置
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;//串口1中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3;//抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority =3;		//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器、
	
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//开启相关中断
	USART_ClearFlag(USART1, USART_FLAG_TC);
	USART_Cmd(USART1, ENABLE);  //使能串口1 
	
	
	//串口2的初始化
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
	
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource2,GPIO_AF_USART2); 
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource3,GPIO_AF_USART2); 
	
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_2 | GPIO_Pin_3; 
	GPIO_Init(GPIOA,&GPIO_InitStructure); 
	
	//USART2 初始化设置
	USART_InitStructure.USART_BaudRate = 19200;//波特率设置
	USART_InitStructure.USART_WordLength = USART_WordLength_9b;//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_Odd;//USART_Parity_No;//USART_Parity_Odd;//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
	USART_Init(USART2, &USART_InitStructure); //初始化串口2

	//Usart2 NVIC 配置
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;//串口2中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3;//抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority =3;		//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器、
	
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);//开启相关中断
	USART_ClearFlag(USART2, USART_FLAG_TC);
	USART_Cmd(USART2, ENABLE);  //使能串口2
	
	
	
	
	//串口3用来做调试输出；(USB转串口芯片)
		USART3_Gpio_Config();
		USARTx_Config(USART3, 115200); 
		
		 
		
		
		USART6_Gpio_Config();
		USART_SBUS_Config(USART_SBUS); 
		
// //中断设置
    USARTxNVIC_Config();		
//	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);//开启相关中断
//	USART_ClearFlag(USART3, USART_FLAG_TC);
//	USART_Cmd(USART3, ENABLE);  //使能串口3
	
	
}
/**********************************************************************************************************
*	函 数 名: UART4_IRQHandler
*	功能说明: 串口4的中断处理函数
*	形    参：
*	返 回 值: 
*	注    释：生命探测仪的通信接口
**********************************************************************************************************/
void USART6_IRQHandler(void)
{
	uint8_t framelen = 0;	//定义可变帧长
    uint8_t paramlen = 0;	//参数长度
	
	if(USART_GetFlagStatus(USART6, USART_FLAG_ORE) != RESET)
	{	
		USART_ClearFlag(USART6, USART_FLAG_ORE); 
		USART_ReceiveData(USART6);	
	}
	
    if(USART_GetITStatus(USART6, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(USART6, USART_IT_RXNE);        //清除中断标志
			sbus_rec_data = USART_ReceiveData(USART6);
			switch(subs_uart_rec_sta)
       {
         case 0:
                if(sbus_rec_data == 0x0f)
                {
                  subs_uart_rec_sta = 1;
                  g_recvSbusBuf[subs_uart_rec_count] = sbus_rec_data;
                  subs_uart_rec_count++;
                }
                else
                {
                  subs_uart_rec_sta = 0;
                  subs_uart_rec_count = 0;
                }
                break;
                
         case 1:
                g_recvSbusBuf[subs_uart_rec_count] = sbus_rec_data;
                
//								if(g_recvSbusBuf[subs_uart_rec_count] == 0x7C){
//									subs_uart_rec_sta = 0;
//                  subs_uart_rec_count = 0;
//									break;
//								}
								subs_uart_rec_count++;
								
				 
                if(subs_uart_rec_count >= 25)
                { 
                   if(g_recvSbusBuf[24] == 0x00)
                   {
                      g_recvSbusTimems = SysTick->VAL;
                      g_recvSbusFlag = 1;
										 
                   }
                   subs_uart_rec_sta = 0;
                   subs_uart_rec_count = 0;
                }
                break;
         
         default:
                break;
       }		
			
		}
	
//	if(USART4_Rxdata >= USART_LIMIT_BUF)
//	{
//		USART4_Rxdata = 0; 
//	}
}	









#endif

