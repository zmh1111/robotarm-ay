/**
  ******************************************************************************
  * @文	件 ： bsp.c
  * @作	者 ： INNFOS Software Team
  * @版	本 ： V1.0.0
  * @日	期 ： 2019.6.14
  * @摘	要 ： 底层驱动初始化
  ******************************************************************************/
  
/* Includes ----------------------------------------------------------------------*/
#include "bsp.h" 

/* Funcation defines -------------------------------------------------------------*/
extern uint16_t sbuschan[18];

extern int angle_flag;
extern int current_angle;


void CameraDriver_Init(void)
{    	 
  GPIO_InitTypeDef  GPIO_InitStructure;

  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);//使能GPIOF时钟

  //GPIOF9,F10初始化设置
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0| GPIO_Pin_1;//LED0和LED1对应IO口
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
  GPIO_Init(GPIOB, &GPIO_InitStructure);//初始化GPIO
	
	GPIO_SetBits(GPIOB,GPIO_Pin_0 | GPIO_Pin_1);//GPIOF9,F10设置高，灯灭

}





/**
  * @功	能	板载LED初始化
  * @参	数	无
  * @返	回	无
  */
void LED_Init(void)
{    	 
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	
	GPIO_ResetBits(GPIOD,GPIO_Pin_2 | GPIO_Pin_3);
}

/**
  * @功	能	底层驱动初始化汇总
  * @参	数	无
  * @返	回	无
  */
void BSP_Init()
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	LED_Init();
	delay_init(180);  		 
	uart_init(115200);
	CameraDriver_Init();
	CAN1_Initializes();
	CAN2_Initializes();	
}




